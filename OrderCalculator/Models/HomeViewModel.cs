﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderCalculator.Models
{
    public class HomeViewModel
    {
        public List<Order> Orders { get; set; }

        public Order NewOrder { get; set; }

        public HomeViewModel()
        {
            Orders = new List<Order>();
        }
    }
}
