﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using OrderCalculator.Models;

namespace OrderCalculator.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View(GetModel());
        }

        [HttpPost]
        public ActionResult Index(HomeViewModel m)
        {
            var model = GetModel();

            if (ModelState.IsValid)
            {

                if (model.Orders.Any(x => x.Id == m.NewOrder.Id))
                {
                    var order = model.Orders.First(x => x.Id == m.NewOrder.Id);
                    order.Input = m.NewOrder.Input;
                    order.Name = m.NewOrder.Name;
                    order.OrderValue = m.NewOrder.OrderValue;
                }
                else
                {
                    if (model.Orders.Any(x => x.Name == m.NewOrder.Name))
                    {
                        Session["errorMessage"] = "Nie można dodać drugiego żarłoka o tym samym imieniu.";
                        return RedirectToAction("Error");
                    }

                    m.NewOrder.Id = Guid.NewGuid().ToString();
                    model.Orders.Add(m.NewOrder);
                }

                model.NewOrder = new Order();
                Session["orders"] = model;

                return RedirectToAction("Index");
            }

            return View(model);
        }

        public ActionResult Delete(string id)
        {
            var model = GetModel();
            var itemToDelete = model.Orders.FirstOrDefault(x => x.Id == id);
            if (itemToDelete != null)
            {
                model.Orders.Remove(itemToDelete);
            }

            return RedirectToAction("Index");
        }

        public ActionResult Edit(string id)
        {
            var model = GetModel();
            model.NewOrder = model.Orders.FirstOrDefault(x => x.Id == id);
            Session["orders"] = model;
            return RedirectToAction("Index");
        }

        public ActionResult Result()
        {
            var model = GetModel();
            double totalPrice = model.Orders.Sum(order => order.OrderValue);
            double totalGathered = model.Orders.Sum(order => order.Input);
            var transfers = new List<Transfer>();
            double leftover = totalGathered - totalPrice;

            if (totalGathered >= totalPrice)
            {
                var lenders = model.Orders.Where(x => x.Input > x.OrderValue).Select(x => new Balance(x.Name, x.Input - x.OrderValue)).ToList();
                var debtors = model.Orders.Where(x => x.Input < x.OrderValue).Select(x => new Balance(x.Name, x.OrderValue - x.Input)).ToList();

                foreach (var lender in lenders)
                {
                    if (leftover > 0)
                    {
                        if (lender.Value <= leftover)
                        {
                            transfers.Add(new Transfer(lender.Name, "[reszta]", lender.Value));
                            leftover -= lender.Value;
                            lender.Value = 0;
                        }
                        else
                        {
                            transfers.Add(new Transfer(lender.Name, "[reszta]", leftover));
                            lender.Value -= leftover;
                            leftover = 0;
                        }
                    }

                    foreach(var debtor in debtors)
                    {
                        if (lender.Value < 0.01)
                        {
                            break;
                        }

                        if (debtor.Value < 0.01)
                        {
                            continue;
                        }

                        if (lender.Value <= debtor.Value)
                        {
                            transfers.Add(new Transfer(lender.Name, debtor.Name, lender.Value));
                            debtor.Value -= lender.Value;
                            lender.Value = 0;
                        }
                        else
                        {
                            transfers.Add(new Transfer(lender.Name, debtor.Name, debtor.Value));
                            lender.Value -= debtor.Value;
                            debtor.Value = 0;
                        }
                    }
                }

                return View(transfers);
            }

            Session["errorMessage"] = "Nie zebrano wystarczająco dużo pieniędzy. Jeszcze pisiont złotych powinno pomóc.";
            return RedirectToAction("Error");
        }

        public ActionResult Error()
        {
            return View();
        }

        public ActionResult Clear()
        {
            var model = GetModel();
            model.Orders.Clear();
            Session["orders"] = model;

            return RedirectToAction("Index");
        }

        private HomeViewModel GetModel()
        {
            return Session["orders"] as HomeViewModel ?? new HomeViewModel();
        }
    }

    class Balance
    {
        public Balance(string name, double value)
        {
            Name = name;
            Value = value;
        }

        public string Name { get; set; }

        public double Value { get; set; }
    }
}